﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="kpmgTransactionData.Webpages.HomePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div style="margin-left: 15px">
        <asp:Image ImageUrl="~/Images/KPMG.png" Height="100" Width="400" runat="server" />

        <h2>Document Manager</h2>
        <div>
            <a href="HomePage.aspx">Home</a>  <a href="FileUploadPage.aspx" style="margin-left: 20px">Upload File</a> <a href="ViewOrEditTransactionData.aspx" style="margin-left: 20px">Transaction Data</a>
        </div>
        <br />
        <br />

        <form id="form1" runat="server">
            <label>To insert new data in the system, click 'Upload new file' button and to view/existing existing data click 'View and edit data' button </label>
            <br />
            <br />
            <div>
                <asp:Button runat="server" ID="UploadNewFile" OnClick="UploadNewFile_Click" Text="Upload new file" Width="150px" />
                <asp:Button runat="server" ID="ViewEditButton" OnClick="ViewEditButton_Click" Text="View and edit data" Width="150px" />
            </div>
        </form>
    </div>
</body>
</html>
