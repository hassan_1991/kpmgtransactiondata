﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileUploadPage.aspx.cs" Inherits="kpmgTransactionData.Webpages.FileUploadPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 349px">
    <div style="margin-left: 15px">
        <asp:Image ImageUrl="~/Images/KPMG.png" Height="100" Width="400" runat="server" />

        <h2>Document Manager</h2>
        <div>
            <a href="HomePage.aspx">Home</a>  <a href="FileUploadPage.aspx" style="margin-left: 20px">Upload File</a> <a href="ViewOrEditTransactionData.aspx" style="margin-left: 20px">Transaction Data</a>
        </div>
         <br />
       
        <form id="form1" runat="server">
            <div>
                <asp:FileUpload ID="FileUploadControl" runat="server" />
                
                <asp:Button runat="server" ID="UploadButton" Text="Upload" OnClick="UploadButton_Click" />
                <br />
                <br />
                <asp:Label runat="server" ID="StatusLabel" />
                <br />
                <br />
            
                <textarea runat="server" id="ErrorsInfo" visible="false" style="min-width: 650px; max-width: 1000px; min-height: 100px; max-height: 250px" />
            </div>
        </form>
    </div>
</body>
</html>
