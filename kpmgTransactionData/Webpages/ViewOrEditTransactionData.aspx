﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewOrEditTransactionData.aspx.cs" Inherits="kpmgTransactionData.Webpages.ViewOrEditTransactionData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div style="margin-left: 15px">
        <asp:Image ImageUrl="~/Images/KPMG.png" Height="100" Width="400" runat="server" />

        <h2>Document Manager</h2>

        <div>
            <a href="HomePage.aspx">Home</a>  <a href="FileUploadPage.aspx" style="margin-left: 20px">Upload File</a> <a href="ViewOrEditTransactionData.aspx" style="margin-left: 20px">Transaction Data</a>
        </div>

        <br />
        <br />

        <form id="form1" runat="server">

            <div style="border-style: solid; border-width: thin; padding: 10px; max-width:1100px" >
                <div style="float: left">
                    <label>Account: </label>
                    <input runat="server" id="AccountFilter" type="text" />
                    <label>Description: </label>
                    <input runat="server" id="DescriptionFilter" type="text" />
                    <label>Currency code: </label>
                    <input runat="server" id="CurrencyCodeFilter" type="text" />
                    <label>Amount: </label>
                    <input runat="server" id="AmountFilter" type="text" />
                </div>
                <br />
                <br />
                <asp:Button ID="FilterQuery" runat="server" Text="Search" OnClick="FilterQuery_Click" Width="100px" />
            </div>

            <h4>Transaction Data</h4>
         
            <asp:SqlDataSource ID="TransactionDataSqlSource" runat="server" ConnectionString="<%$ ConnectionStrings:kpmgConnectionString %>" 
                SelectCommand="getAllRecords" SelectCommandType="StoredProcedure" UpdateCommand="updateTransactionData" UpdateCommandType="StoredProcedure">
               
            </asp:SqlDataSource>
            
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowEditing="GridView1_RowEditing" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowUpdating="GridView1_RowUpdating" OnPageIndexChanging="GridView1_PageIndexChanging" DataKeyNames="id" DataSourceID="TransactionDataSqlSource" AllowPaging="True" AllowSorting="True" Width="920px" PageSize="20" style="margin-right: 93px">
                <Columns>
                    <asp:CommandField ShowEditButton="True" />

                    <asp:TemplateField HeaderText="Account" SortExpression="account">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("account") %>'></asp:TextBox>
                        </EditItemTemplate>

                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("account") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle Width="150px" />
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" SortExpression="description">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("description") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2"  runat="server" Text='<%# Bind("description") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Width="450px" />
                        <FooterStyle Width="450px" />
                        <HeaderStyle Width="450px" />
                        <ItemStyle Width="450px" Wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Currency" SortExpression="currencyIsoCode">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("currencyIsoCode") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("currencyIsoCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount" SortExpression="amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("amount") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate >
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="isDeleted" HeaderText="Removed" SortExpression="isDeleted"/>
                </Columns>
            </asp:GridView>
        </form>
    </div>

</body>
</html>
