﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kpmgTransactionData.Webpages
{
    public partial class ViewOrEditTransactionData : System.Web.UI.Page
    {
        const string account = "account";
        const string description = "description";
        const string currencyIsoCode = "currencyIsoCode";
        const string amount = "amount";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void FilterQuery_Click(object sender, EventArgs e)
        {
            TransactionDataSqlSource.SelectParameters.Clear();

            SearchType searchType = GetSearchType();

            if (searchType == SearchType.None)
                SetSqlDataSource(StoredProcedures.getAllRecords, new List<Tuple<string, string>>());

            else if (searchType == SearchType.Account)
                SetSqlDataSource(StoredProcedures.accountFilterQuery, new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)) });

            else if (searchType == SearchType.Description)
                SetSqlDataSource(StoredProcedures.descriptionFilterQuery, new List<Tuple<string, string>>() { (Tuple.Create(description, DescriptionFilter.Value)) });

            else if (searchType == SearchType.Currency)
                SetSqlDataSource(StoredProcedures.currencyIsoCodeFilterQuery, new List<Tuple<string, string>>() { (Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)) });

            else if (searchType == SearchType.Amount)
                SetSqlDataSource(StoredProcedures.amountFilterQuery, new List<Tuple<string, string>>() { (Tuple.Create(amount, AmountFilter.Value)) });

            else if (searchType == SearchType.AccountDescription)
                SetSqlDataSource(StoredProcedures.accountAndDescriptionFilterQuery,
                    new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)), (Tuple.Create(description, DescriptionFilter.Value)) });

            else if (searchType == SearchType.AccountCurrency)
                SetSqlDataSource(StoredProcedures.accountAndCurrencyIsoCodeFilterQuery,
                    new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)), (Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)) });

            else if (searchType == SearchType.AccountAmount)
                SetSqlDataSource(StoredProcedures.accountAndAmountFilterQuery,
                    new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)), (Tuple.Create(amount, AmountFilter.Value)) });

            else if (searchType == SearchType.AccountDescriptionCurrency)
                SetSqlDataSource(StoredProcedures.accountDescriptionCurrencyCodeFilterQuery,
                   new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)), (Tuple.Create(description, DescriptionFilter.Value)), (Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)) });

            else if (searchType == SearchType.AccountCurrencyAmount)
                SetSqlDataSource(StoredProcedures.accountCurrencyCodeAmountFilterQuery,
                   new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)), (Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)), (Tuple.Create(amount, AmountFilter.Value)) });

            else if (searchType == SearchType.AccountDescriptionAmount)
                SetSqlDataSource(StoredProcedures.accountDescriptionAmountFilterQuery,
                    new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)), (Tuple.Create(description, DescriptionFilter.Value)), (Tuple.Create(amount, AmountFilter.Value)) });


            else if (searchType == SearchType.AllFields)
                SetSqlDataSource(StoredProcedures.accountDescriptionCurrencyCodeAmountFilterQuery,
                   new List<Tuple<string, string>>() { (Tuple.Create(account, AccountFilter.Value)),
                       (Tuple.Create(description, DescriptionFilter.Value)),(Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)), (Tuple.Create(amount, AmountFilter.Value)) });

            else if (searchType == SearchType.DescriptionCurrency)
                SetSqlDataSource(StoredProcedures.descriptionAndCurrencyIsoCodeFilterQuery,
                   new List<Tuple<string, string>>() {
                       (Tuple.Create(description, DescriptionFilter.Value)),(Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)) });

            else if (searchType == SearchType.DescriptionAmount)
                SetSqlDataSource(StoredProcedures.descriptionAmountFilterQuery,
                   new List<Tuple<string, string>>() {
                       (Tuple.Create(description, DescriptionFilter.Value)), (Tuple.Create(amount, AmountFilter.Value)) });

            else if (searchType == SearchType.DescriptionCurrencyAmount)
                SetSqlDataSource(StoredProcedures.descriptionCurrencyCodeAmountFilterQuery,
                new List<Tuple<string, string>>() {
                       (Tuple.Create(description, DescriptionFilter.Value)),(Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)), (Tuple.Create(amount, AmountFilter.Value)) });

            else if (searchType == SearchType.CurrencyAmount)
                SetSqlDataSource(StoredProcedures.currencyCodeAmountFilterQuery,
                new List<Tuple<string, string>>() { (Tuple.Create(currencyIsoCode, CurrencyCodeFilter.Value)), (Tuple.Create(amount, AmountFilter.Value)) });

        }

        private void SetSqlDataSource(string selectCommand, List<Tuple<string, string>> storedProcedureParameters)
        {
            TransactionDataSqlSource.SelectCommand = selectCommand;

            foreach (var parameter in storedProcedureParameters)
                TransactionDataSqlSource.SelectParameters.Add(parameter.Item1, parameter.Item2.Trim());
        }

        private SearchType GetSearchType()
        {
            if (string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value) && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.None;

            else if (!string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value) && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.Account;

            else if (string.IsNullOrWhiteSpace(AccountFilter.Value) && !string.IsNullOrWhiteSpace(DescriptionFilter.Value) && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.Description;

            else if (string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value) && !string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.Currency;

            else if (string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value)
               && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && !string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.Amount;

            else if (!string.IsNullOrWhiteSpace(AccountFilter.Value) && !string.IsNullOrWhiteSpace(DescriptionFilter.Value)
               && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.AccountDescription;

            else if (!string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value)
               && !string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.AccountCurrency;

            else if (!string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value)
              && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && !string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.AccountAmount;

            else if (!string.IsNullOrWhiteSpace(AccountFilter.Value) && !string.IsNullOrWhiteSpace(DescriptionFilter.Value)
              && !string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.AccountDescriptionCurrency;

            else if (!string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value)
              && !string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && !string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.AccountCurrencyAmount;


            else if (!string.IsNullOrWhiteSpace(AccountFilter.Value) && !string.IsNullOrWhiteSpace(DescriptionFilter.Value)
              && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && !string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.AccountDescriptionAmount;


            else if (string.IsNullOrWhiteSpace(AccountFilter.Value) && !string.IsNullOrWhiteSpace(DescriptionFilter.Value)
             && !string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.DescriptionCurrency;

            else if (string.IsNullOrWhiteSpace(AccountFilter.Value) && !string.IsNullOrWhiteSpace(DescriptionFilter.Value)
            && string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && !string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.DescriptionAmount;

            else if (string.IsNullOrWhiteSpace(AccountFilter.Value) && !string.IsNullOrWhiteSpace(DescriptionFilter.Value)
            && !string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && !string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.DescriptionCurrencyAmount;

            else if (string.IsNullOrWhiteSpace(AccountFilter.Value) && string.IsNullOrWhiteSpace(DescriptionFilter.Value)
            && !string.IsNullOrWhiteSpace(CurrencyCodeFilter.Value) && !string.IsNullOrWhiteSpace(AmountFilter.Value))
                return SearchType.CurrencyAmount;

            return SearchType.AllFields;
        }

        public enum SearchType
        {
            None, AllFields, Account, Description, Currency, Amount, AccountDescription, AccountCurrency, AccountAmount, AccountDescriptionCurrency,
            AccountDescriptionAmount, AccountCurrencyAmount, DescriptionAmount, DescriptionCurrency, DescriptionCurrencyAmount, CurrencyAmount
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TransactionDataSqlSource.SelectParameters.Clear();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            TransactionDataSqlSource.SelectParameters.Clear();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            TransactionDataSqlSource.SelectParameters.Clear();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TransactionDataSqlSource.SelectParameters.Clear();
        }
    }
}