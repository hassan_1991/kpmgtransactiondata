﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace kpmgTransactionData.Webpages
{
    public partial class FileUploadPage : System.Web.UI.Page
    {
        bool IsPageRefresh;

        const int descriptionMaxLenght = 100;
        const int accountMaxLenght = 100;
        const int expectedNumberOfColumns = 4;

        const int accountColumnIndex = 1;
        const int descriptionColumnIndex = 2;
        const int currencyCodeColumnIndex = 3;
        const int amountColumnIndex = 4;

        const string accountColumn = "account";
        const string descriptionColumn = "description";
        const string currencyCodeColumn = "currency code";
        const string amountColumn = "amount";
        const string invalidTransactionsDataFolder = "invalidTransactionDataFiles";
        const string uploadedFilesFolder = "uploadedFiles";
        const string validExcelExtension = ".xlsx";

        StringBuilder sbErrors;

        private List<TransactionData> validTransactionData;
        private List<TransactionData> inValidTransactionData;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["postids"] = System.Guid.NewGuid().ToString();
                Session["postid"] = ViewState["postids"].ToString();
            }
            else
            {
                if (ViewState["postids"].ToString() != Session["postid"].ToString())
                    IsPageRefresh = true;

                Session["postid"] = Guid.NewGuid().ToString();
                ViewState["postids"] = Session["postid"].ToString();
            }
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (IsPageRefresh)
                return;

            Initialise();

            if (FileUploadControl.HasFile)
            {
                try
                {
                    if (IsValidFileType())
                    {
                        string filePath = Server.MapPath($"~/{uploadedFilesFolder}/") + DateTimeStamp() + "-" + Path.GetFileName(FileUploadControl.FileName); ;

                        FileUploadControl.SaveAs(filePath);

                        ProcessPostedFileByOleDbConnection(filePath);

                        StoreValidTransactionDataInDb();

                        StoreInvalidTransactionData();

                        if (sbErrors.Length != 0)
                            DisplayErrorBox(sbErrors);
                    }
                    else
                        SetStatusLabelText($"Upload status: Only {validExcelExtension} files are accepted!");
                }
                catch (Exception ex)
                {
                    SetStatusLabelText("Upload status: The file could not be uploaded. The following error occured: " + ex.Message);
                }
            }
        }

        private string DateTimeStamp()
        {
            return DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss").ToString();
        }

        private void SetStatusLabelText(string text)
        {
            StatusLabel.Text = text;
        }

        private void StoreInvalidTransactionData()
        {
            //todo store the invalid records in excel file in foldeer: invalidTransactionDataFiles
            //and provide the download link for users 
        }

        private bool IsValidFileType()
        {
            return Path.GetExtension(FileUploadControl.PostedFile.FileName) == validExcelExtension;
        }

        private void Initialise()
        {
            if (!Directory.Exists(Server.MapPath($"~/{uploadedFilesFolder}/")))
                Directory.CreateDirectory(Server.MapPath($"~/{uploadedFilesFolder}/"));
            
            ErrorsInfo.Visible = false;
            validTransactionData = new List<TransactionData>();
            inValidTransactionData = new List<TransactionData>();
        }

        private void ProcessPostedFileByOleDbConnection(string filePath)
        {
            DateTime startTime = DateTime.Now;
            sbErrors = new StringBuilder();

            string con = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";" + "Extended Properties='Excel 12.0 Xml;HDR=NO;'";
            try
            {
                using (OleDbConnection connection = new OleDbConnection(con))
                {
                    connection.Open();
                    OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", connection);
                    int counter = 0;
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            try
                            {
                                counter++;

                                string account = dr[0].ToString();
                                string description = dr[1].ToString();
                                string currencyCode = dr[2].ToString();

                                List<string> inValidField = Validate(account, description, currencyCode);

                                double amount = 0;
                                var test = dr[3].ToString();
                                if (!double.TryParse(dr[3].ToString().ToString(), out amount))
                                    inValidField.Add(amountColumn);

                                if (!inValidField.Any())
                                    validTransactionData.Add(new TransactionData(account, description, currencyCode, amount, true));
                                else
                                {
                                    inValidTransactionData.Add(new TransactionData(account, description, currencyCode, amount, false));
                                    AddInvalidRowErrorMessage(sbErrors, inValidField, counter);
                                }
                            }
                            catch (Exception ex)
                            {
                                sbErrors.AppendLine(ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                sbErrors.AppendLine(ex.Message);
            }

            Debug.WriteLine($"processing {validTransactionData.Count} records took: " + (DateTime.Now - startTime).TotalSeconds);
        }

        private List<string> Validate(string account, string description, string currencyCode)
        {
            List<string> errorList = new List<string>();

            if (!IsValidAccount(account))
                errorList.Add(accountColumn);

            if (!IsValidDescription(description))
                errorList.Add(descriptionColumn);

            if (!IsValidCurrencyCode(currencyCode))
                errorList.Add(currencyCodeColumn);

            return errorList;
        }

        private void StoreValidTransactionDataInDb()
        {
            DateTime startTime = DateTime.Now;
            int counter = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kpmgConnectionString"].ConnectionString))
            {
                conn.Open();

                foreach (TransactionData td in validTransactionData)
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand(StoredProcedures.insertTransactionData, conn);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@account", td.Account);
                        cmd.Parameters.AddWithValue("@description", td.Description);
                        cmd.Parameters.AddWithValue("@currencyIsoCode", td.CurrencyCode);
                        cmd.Parameters.AddWithValue("@amount", td.Amount);
                        cmd.ExecuteNonQuery();
                        counter++;
                    }
                    catch (Exception ex)
                    {
                        sbErrors.AppendLine("Error during saving in DB: " + ex.Message);
                    }
                }

                conn.Close();
            }

            SetStatusLabelText($"Total rows: {inValidTransactionData.Count + validTransactionData.Count}. "
                                      + $"Invalid rows: {inValidTransactionData.Count + validTransactionData.Count - counter}. Valid rows saved in db: {counter}.");

            Debug.WriteLine($"storing {validTransactionData.Count} records in db took: " + (DateTime.Now - startTime).TotalSeconds);
        }

        private void DisplayErrorBox(StringBuilder sb)
        {
            ErrorsInfo.Visible = true;

            ErrorsInfo.InnerText = sb.ToString();
        }

        private void AddInvalidRowErrorMessage(StringBuilder sb, List<string> inValidColumns, int row)
        {
            sb.AppendLine($"Row number {row} failed, invalid fields: {string.Join(",", inValidColumns)}.");
        }

        private bool IsValidAccount(string account)
        {
            return !string.IsNullOrWhiteSpace(account) && account.Length <= accountMaxLenght;
        }

        private bool IsValidDescription(string description)
        {
            return !string.IsNullOrWhiteSpace(description) && description.Length <= descriptionMaxLenght;
        }

        private bool IsValidCurrencyCode(string currencyCode)
        {
            return !string.IsNullOrWhiteSpace(currencyCode) && Currencies.IsValidCurrency(currencyCode);
        }
    }
}