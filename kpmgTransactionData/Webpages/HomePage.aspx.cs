﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kpmgTransactionData.Webpages
{
    public partial class HomePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UploadNewFile_Click(object sender, EventArgs e)
        {
            Response.Redirect("FileUploadPage.aspx");
        }

        protected void ViewEditButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewOrEditTransactionData.aspx");
        }
    }
}