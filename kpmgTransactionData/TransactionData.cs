﻿namespace kpmgTransactionData
{
    public class TransactionData
    {
        public TransactionData(string account, string description, string currencyCode, double amount, bool isValid)
        {
            Account = account;
            Description = description;
            CurrencyCode = currencyCode;
            Amount = amount;
            IsValid = isValid;
        }

        public string Account { get; private set; }
        public string Description { get; private set; }
        public string CurrencyCode { get; private set; }
        public double? Amount { get; private set; }
        public bool IsValid { get; private set; }
    }
}