﻿namespace kpmgTransactionData
{
    public class StoredProcedures
    {
        public const string insertTransactionData = "insertTransactionData";
        public const string getAllRecords = "getAllRecords";
        public const string accountAndCurrencyIsoCodeFilterQuery = "accountAndCurrencyIsoCodeFilterQuery";
        public const string accountAndAmountFilterQuery = "accountAndAmountFilterQuery";
        public const string accountAndDescriptionFilterQuery = "accountAndDescriptionFilterQuery";
        public const string accountCurrencyCodeAmountFilterQuery = "accountCurrencyCodeAmountFilterQuery";
        public const string accountDescriptionAmountFilterQuery = "accountDescriptionAmountFilterQuery";
        public const string accountDescriptionCurrencyCodeAmountFilterQuery = "accountDescriptionCurrencyCodeAmountFilterQuery";
        public const string accountDescriptionCurrencyCodeFilterQuery = "accountDescriptionCurrencyCodeFilterQuery";
        public const string accountFilterQuery = "accountFilterQuery";
        public const string amountFilterQuery = "amountFilterQuery";
        public const string currencyCodeAmountFilterQuery = "currencyCodeAmountFilterQuery";
        public const string currencyIsoCodeFilterQuery = "currencyIsoCodeFilterQuery";
        public const string descriptionAmountFilterQuery = "descriptionAmountFilterQuery";
        public const string descriptionAndCurrencyIsoCodeFilterQuery = "descriptionAndCurrencyIsoCodeFilterQuery";
        public const string descriptionCurrencyCodeAmountFilterQuery = "descriptionCurrencyCodeAmountFilterQuery";
        public const string descriptionFilterQuery = "descriptionFilterQuery";
    }
}